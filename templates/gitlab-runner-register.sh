#!/bin/bash
set -xue

# set booleans
export REGISTER_LEAVE_RUNNER=false
export REGISTER_RUN_UNTAGGED=false
export REGISTER_LOCKED=true
export DOCKER_PRIVILEGED=true

gitlab-runner register \
    --non-interactive \
    --registration-token {{item.REGISTRATION_TOKEN}} \
    --url {{item.CI_SERVER_URL|default(CI_SERVER_URL)}} \
    --tag-list {{item.RUNNER_TAG_LIST|default(RUNNER_TAG_LIST)}} \
    --config {{CONFIG_FILE|default('/etc/gitlab-runner/config.toml')}} \
    --executor {{RUNNER_EXECUTOR|default('docker')}} \
    --limit {{RUNNER_LIMIT|default(5)}} \
    --request-concurrency {{RUNNER_LIMIT|default(5)}} \
    --maximum-timeout {{REGISTER_MAXIMUM_TIMEOUT|default(36000)}} \
    --output-limit {{RUNNER_OUTPUT_LIMIT|default(8192)}} \
    --docker-image {{DOCKER_IMAGE|default('alpine:latest')}} \
{% if RUNNER_EXECUTOR == 'docker+machine' %}
    --machine-machine-driver {{MACHINE_DRIVER}} \
    --machine-machine-name {{MACHINE_NAME|default('%s')}} \
    --machine-max-builds {{MACHINE_MAX_BUILDS|default(1)}} \
    --machine-machine-options "engine-install-url={{MACHINE_DOCKER_INSTALL_URL}}" \
    --machine-machine-options "engine-storage-driver=devicemapper" \
{% if MACHINE_DRIVER == 'openstack' %}
    --machine-machine-options "openstack-auth-url={{OS_AUTH_URL}}" \
    --machine-machine-options "openstack-username={{OS_USERNAME}}" \
    --machine-machine-options "openstack-password={{OS_PASSWORD}}" \
    --machine-machine-options "openstack-domain-name={{OS_DOMAIN_NAME}}" \
    --machine-machine-options "openstack-region={{OS_REGION_NAME}}" \
    --machine-machine-options "openstack-tenant-name={{OS_TENANT_NAME}}" \
    --machine-machine-options "openstack-net-name={{OS_NETWORK_NAME}}" \
    --machine-machine-options "openstack-sec-groups={{OS_SECURITY_GROUPS}}" \
    --machine-machine-options "openstack-user-data-file={{OS_USER_DATA_FILE}}" \
    --machine-machine-options "openstack-ssh-user={{OS_SSH_USER}}" \
    {% if 'samba-ci-private' in item.RUNNER_TAG_LIST %}
    --machine-machine-options "openstack-flavor-name={{OS_FLAVOR_NAME}}" \
    {% else %}
    --machine-machine-options "openstack-flavor-name=c1.c2r16" \
    {% endif %}
    {% if OS_IMAGE_ID is defined %}
    --machine-machine-options "openstack-image-id={{OS_IMAGE_ID}}" \
    {% else %}
    --machine-machine-options "openstack-image-name={{OS_IMAGE_NAME}}" \
    {% endif %}
{% elif MACHINE_DRIVER == 'rackspace' %}
    --machine-machine-options "rackspace-username={{OS_USERNAME}}" \
    --machine-machine-options "rackspace-api-key={{OS_API_KEY}}" \
    --machine-machine-options "rackspace-region={{OS_REGION_NAME}}" \
    --machine-machine-options "rackspace-ssh-user={{OS_SSH_USER}}" \
    {% if 'samba-ci-private' in item.RUNNER_TAG_LIST %}
    --machine-machine-options "rackspace-flavor-id={{OS_FLAVOR_ID}}" \
    {% else %}
    --machine-machine-options "rackspace-flavor-id=7" \
    {% endif %}
    --machine-machine-options "rackspace-image-id={{OS_IMAGE_ID}}" \
{% endif %}
{% endif %}
    "$@"

# for new tag samba-ci-private, use ubuntu1804 and 8G RAM
# for legacy, use ubuntu1804 and 16G RAM
